package repository

import (
	"context"
	"database/sql"
	"github.com/Sirupsen/logrus"
	"local.seward.gd/models"
	"local.seward.gd/national"
)

type mysqlNationalRepository struct {
	MasterConn *sql.DB
	SlaveConn *sql.DB
}

//NewMysqlNationalRepository will create an object that represent the national.Repository interface
func NewMysqlNationalRepository(masterConn *sql.DB, slaveConn *sql.DB) national.Repository {
	return &mysqlNationalRepository{masterConn, slaveConn}
}

func (m *mysqlNationalRepository) fetch(ctx context.Context, query string) ([]*models.National, error) {
	rows, err := m.SlaveConn.QueryContext(ctx, query)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	defer func() {
		err := rows.Close()
		if err != nil {
			logrus.Error(err)
		}
	}()

	result := make([]*models.National, 0)
	for rows.Next() {
		t := new(models.National)
		err = rows.Scan(
			&t.Country,
			)

		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		result = append(result, t)
	}
	return result, nil
}

func (m *mysqlNationalRepository) Fetch(ctx context.Context) ([]*models.National, error) {
	query := `SELECT country FROM nationalities`

	res, err := m.fetch(ctx, query)
	if err != nil {
		return nil, err
	}

	return res, err
}