package http

import (
	"context"
	"github.com/labstack/echo"
	"local.seward.gd/models"
	"local.seward.gd/national"
	"local.seward.gd/utils"
	"net/http"
)

const object = "nationalities"

//NationalHandler represent the httpHandler for national
type NationalHandler struct {
	NUsecase national.Usecase
	SGError models.SGError
}

//NewNationalHandler will initialize the national / resources endpoint
func NewNationalHandler(e *echo.Echo, us national.Usecase)  {
	handler := &NationalHandler{
		NUsecase: us,
	}

	e.GET("/nationalities", handler.FetchNationalities)
}

func (m *NationalHandler) FetchNationalities(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	listAr, err := m.NUsecase.Fetch(ctx)

	if err != nil {
		m.SGError.Message = err.Error()
		return c.JSON(utils.GetStatusCode(err), models.ResponseError{Error: m.SGError})
	}
	return c.JSON(http.StatusOK, listAr)
}