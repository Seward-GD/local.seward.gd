package national

import (
	"context"
	"local.seward.gd/models"
)

type Usecase interface {
	Fetch(c context.Context) ([]models.NullString, error)
}