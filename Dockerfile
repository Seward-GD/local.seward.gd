#Builder
FROM golang:1.14.2-alpine3.11

RUN apk update && apk upgrade && \
    apk --update add git make

#Set the current working directory inside the container
WORKDIR $GOPATH/src/local.seward.gd

#Copy everything from the current directory to the PWD (Present Working Directory) inside the container
COPY . .

RUN go get -d ./...
RUN go install ./...
RUN go build

RUN ls -l
RUN pwd
EXPOSE 9090

CMD["local.seward.gd"]