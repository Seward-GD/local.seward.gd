package db

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/spf13/viper"
)

// GetMasterDB func create db connection
func GetMasterDB() *sql.DB {
	dbHost := viper.GetString(`database.master.host`)
	dbPort := viper.GetString(`database.master.port`)
	dbUser := viper.GetString(`database.master.user`)
	dbPass := viper.GetString(`database.master.pass`)
	dbName := viper.GetString(`database.master.dbname`)
	return getDBConnection(dbHost, dbPort, dbUser, dbPass, dbName)
}

// GetSlaveDB func create db connection
func GetSlaveDB() *sql.DB {
	dbHost := viper.GetString(`database.slave.host`)
	dbPort := viper.GetString(`database.slave.port`)
	dbUser := viper.GetString(`database.slave.user`)
	dbPass := viper.GetString(`database.slave.pass`)
	dbName := viper.GetString(`database.slave.dbname`)
	return getDBConnection(dbHost, dbPort, dbUser, dbPass, dbName)
}

// GetConditionFromMapCondition convert map to where string
func GetConditionFromMapCondition(mapCondition map[string]string) string {
	condition := ""
	for _, v := range mapCondition {
		condition += v + " AND "
	}

	return strings.TrimRight(condition, " AND ")
}

// getDBConnection func create db connection
func getDBConnection(dbHost string, dbPort string, dbUser string, dbPass string, dbName string) *sql.DB {
	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)
	val := url.Values{}
	val.Add("parseTime", "1")
	val.Add("loc", "Asia/Jakarta")
	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())
	dbConn, err := sql.Open(`mysql`, dsn)
	if err != nil && viper.GetBool("debug") {
		fmt.Println(err)
	}
	err = dbConn.Ping()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	return dbConn
}