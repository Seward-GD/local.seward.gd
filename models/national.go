package models

//national represent the national model
type National struct {
	Country  		 NullString     `json:"country,omitempty" validate:"required"`
	Noun    	 	 NullString     `json:"noun"`
	Adj     		 NullString     `json:"adj"`
	Alpha2  		 NullString 	`json:"alpha_2"`
	Alpha3  		 NullString 	`json:"alpha_3"`
	CountryCode  	int64 			`json:"country_code,omitempty" validate:"required"`
}
