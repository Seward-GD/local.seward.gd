package models

type SGError struct {
	Message string `json:"message"`
}

// ResponseError represent the reseponse error struct
type ResponseError struct {
	Error SGError `json:"error"`
}
